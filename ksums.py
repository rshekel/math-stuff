import sys
import itertools

SUM_OPTIONS = {}

def populate_sum_options(k):
    # Not including sums with 1,2 because they will always be in list... 
    for x in range(3,k):
        opts = []
        for i in range(3, int(x/2) + 1):
            opts.append((i, x-i))
        SUM_OPTIONS[x] = list(opts)

def is_good(l):
    for i in l:
        for x,y in SUM_OPTIONS[i]:
            if x not in l and y not in l:
                return False
        
    return True 

def gen_options(k):
    max_num = 2*k - 1
    # choose k-2 numbers between 3 and 2k-1
    options = itertools.combinations(range(3,max_num + 1), k-2)
    return options

def get_options(k):
    populate_sum_options(2*k+1)
    options = gen_options(k)
    goods = filter(is_good, options)
    l = list(goods)
    return l

def get_amounts(max_k=10):
    for i in range(3, max_k):
        options = get_options(i)
        print("{num}: {amount}".format(num=i, amount=len(options)))
